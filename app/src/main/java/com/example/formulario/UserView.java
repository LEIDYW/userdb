package com.example.formulario;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserView extends RecyclerView.ViewHolder {

    TextView nombre;

    //metodo constructor
    public UserView@NonNull View itemView {
        super(itemView);
        nombre=itemView.findViewById(R.id.nombre);
    }

    public UserView(@NonNull android.view.View itemView) {
        super(itemView);
    }
}
